using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponManger : MonoBehaviour
{
    public GameObject gun1;
    public GameObject gun2;
    public GameObject gun3;
    public GameObject crosshair;
    public GameObject reloadLightFire;
    public GameObject reloadLightNoFire;
    public vThirdPersonCamera thirdPersonCamera;
    public Text moneyCounter;
    public bool waitForReload = false;
    public bool hasGun1 = false;
    public bool hasGun2 = false;
    public bool hasGun3 = false;
    public bool selectedGun1 = false;
    public bool selectedGun2 = false;
    public bool selectedGun3 = false;
    public float playerMoney = 0;

    public GameObject projectilePrefab1;
    public float projectileSpeed1 = 1000;
    public Transform firingPoint1;
    public Transform rifleTransform;

    public GameObject projectilePrefab2;
    public float projectileSpeed2 = 1000;
    public Transform firingPoint2;
    public Transform rifleTransform2;

    private bool isAiming;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        //Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        gun1.gameObject.SetActive(false);
        gun2.gameObject.SetActive(false);
        //gun3.gameObject.SetActive(false);
        reloadLightFire.SetActive(true);
        reloadLightNoFire.SetActive(false);
    }

    public void PickupOneCoin()
    {
        playerMoney = playerMoney + 1;
        moneyCounter.text = "Money: " + playerMoney.ToString();
    }

    public void Gun1Handle()
    {
        hasGun1 = true;
    }

    public void Gun2Handle()
    {
        hasGun2 = true;
    }

    public void Reload()
    {
        waitForReload = false;
        reloadLightFire.SetActive(true);
        reloadLightNoFire.SetActive(false);
    }
    
    
    void Update()
    {
        var target = (rifleTransform.position - Camera.main.transform.position);
        rifleTransform.rotation = Quaternion.LookRotation(Camera.main.transform.up, target);

        var target2 = (rifleTransform2.position - Camera.main.transform.position);
        rifleTransform2.rotation = Quaternion.LookRotation(Camera.main.transform.up, target);

        //##########################
        //### Gun Equip Controls ###
        //##########################

        if (Input.GetKeyDown(KeyCode.Alpha1) && hasGun1 == true)
        {
            gun2.gameObject.SetActive(false);
            selectedGun2 = false;
            //
            gun1.gameObject.SetActive(true);
            selectedGun1 = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && hasGun2 == true)
        {
            gun1.gameObject.SetActive(false);
            selectedGun1 = false;
            //
            gun2.gameObject.SetActive(true);
            selectedGun2 = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {

        }

        if (selectedGun1 == true && isAiming == true && waitForReload == false && Input.GetButtonDown("Fire1"))
        {
            GameObject projectileIstance1;
            projectileIstance1 = Instantiate(projectilePrefab1, firingPoint1.transform.position, firingPoint1.transform.rotation);
            projectileIstance1.GetComponent<Rigidbody>().AddForce(firingPoint1.forward * projectileSpeed1);
            Destroy(projectileIstance1, 5);
            reloadLightFire.SetActive(false);
            reloadLightNoFire.SetActive(true);
            waitForReload = true;
            Invoke("Reload", 0.1f);
        }

        if (selectedGun2 == true && isAiming == true && waitForReload == false && Input.GetButton("Fire1"))
        {
            GameObject projectileIstance2;
            projectileIstance2 = Instantiate(projectilePrefab2, firingPoint2.transform.position, firingPoint2.transform.rotation);
            projectileIstance2.GetComponent<Rigidbody>().AddForce(firingPoint2.forward * projectileSpeed1);
            Destroy(projectileIstance2, 5);
        }

        //#####################################
        //### Camera Aiming And Mouse State ###
        //#####################################

        if (Input.GetButton("Fire2"))
        {
            //thirdPersonCamera.lockCamera = true;
            thirdPersonCamera.rightOffset = 0.7f;
            thirdPersonCamera.defaultDistance = 0.7f;
            thirdPersonCamera.height = 1.6f;
            isAiming = true;
            crosshair.SetActive(true);
        }
        else
        {
            //thirdPersonCamera.lockCamera = false;
            thirdPersonCamera.rightOffset = 0f;
            thirdPersonCamera.defaultDistance = 2f;
            thirdPersonCamera.height = 1.8f;
            isAiming = false;
            crosshair.SetActive(false);
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        if (Input.GetKey(KeyCode.Tilde))
        {
            //Cursor.lockState = CursorLockMode.Locked;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
        }
    }
}


/*
 
            GameObject projectileIstance;
            projectileIstance = Instantiate(projectilePrefab1, firingPoint1.position, firingPoint1.rotation);
            projectileIstance.GetComponent<Rigidbody>().AddForce(firingPoint1.forward * projectileSpeed1);
            Destroy(projectileIstance, 5);

*/
