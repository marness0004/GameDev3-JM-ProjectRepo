using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinPickup : MonoBehaviour
{
    public WeaponManger player;
    bool isCollected = false;

    void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<WeaponManger>();
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && isCollected == false)
        {
            isCollected = true;
            player.PickupOneCoin();
            Destroy(this.gameObject);
        }
    }
}
