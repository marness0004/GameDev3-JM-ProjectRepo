using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPress : MonoBehaviour
{
    public bool weaponTwo;
    public bool weaponThree;
    public float weaponTwoCost;
    public WeaponManger moneyCount;

    public Material red;

    void Start()
    {
        moneyCount = gameObject.GetComponent<WeaponManger>();    
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            Debug.Log("Button");
            if (weaponTwo == true && moneyCount.playerMoney >= weaponTwoCost)
            {
                this.gameObject.GetComponent<Renderer>().material = red;
                moneyCount.hasGun2 = true;
                moneyCount.Gun2Handle();
                weaponTwo = false;
            }
        }
    }


}
