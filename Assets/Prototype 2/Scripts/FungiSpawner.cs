using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FungiSpawner : MonoBehaviour
{

    public GameObject fungiType1;
    public CountdownTimer timer;
    public Transform[] spawnPoints;

    public Text countdownDisplay;
    public Text waveNumber;
    private float waveCounter = 0;
    private int minAmount;
    private int maxAmount;


    public void Invasion()
    {
        waveCounter++;
        waveNumber.text = "Wave " + waveCounter.ToString();
        WaveCheck();
        SpawnFungiCal();
    }

    public void SpawnFungi()
    {
        int spawn = Random.Range(0, spawnPoints.Length);
        GameObject fungiInstance;
        fungiInstance = Instantiate(fungiType1, spawnPoints[spawn].transform.position, Quaternion.identity);
    }

    public void WaveCheck()
    {
        if (waveCounter == 1)
        {
            minAmount = 3;
            maxAmount = 5;
        }
        if (waveCounter == 2)
        {
            minAmount = 5;
            maxAmount = 10;
        }
        if (waveCounter == 3)
        {
            minAmount = 10;
            maxAmount = 20;
        }
    }


    public void SpawnFungiCal()
    {

        int spawnCount = Random.Range(minAmount, maxAmount);
        for (int i = 0; i < spawnCount; i++)
        {
            Invoke("SpawnFungi", Random.Range(2.0f, 10.0f));
        }
    }
}

/*int spawnCount = Random.Range(5, 10);
for (int i = 0; i < spawnCount; i++)
{
    Invoke("SpawnFungi",  Random.Range( 1.0f, 10.0f));
}*/
