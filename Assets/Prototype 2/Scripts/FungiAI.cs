using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FungiAI : MonoBehaviour
{
    NavMeshAgent nmHuntTreeAI;
    public Transform target;
    public TreeHP theTree;
    public GameObject coinPrefab;

    void Start()
    {
        nmHuntTreeAI = GetComponent<NavMeshAgent>();
        theTree = GameObject.FindWithTag("Target").GetComponent<TreeHP>();
        isDead = false;
    }
    
    void Update()
    {
            nmHuntTreeAI.SetDestination(SpecialTarget.target.position);
        
    }

    bool isDead = false;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9 && isDead == false)
        {
            isDead = true;
            GameObject coinPickup;
            coinPickup = Instantiate(coinPrefab, new Vector3(transform.position.x, 2, transform.position.z), transform.rotation);
            Destroy(this.gameObject);
        }

    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer == 8 && isDead == false)
        {
            isDead = true;
            theTree.TreeHit();
            Destroy(this.gameObject);
        }
    }
}
