using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{

    public float countdownTime;
    public float countdownNextWave;
    public Text countdownDisplay;
    public GameObject enemyManger;

    public void Update()
    {
        countdownTime -= Time.deltaTime;

        if (countdownTime < 0.0f)
        {
            enemyManger.GetComponent<FungiSpawner>().Invasion();
            ResetTimer();
        }

        countdownDisplay.text = countdownTime.ToString();
    }

    public void ResetTimer()
    {
        countdownTime = countdownNextWave;
    }


/*    
    public IEnumerator CountdownToWaveTwo()
    {    
        while (countdownNextWave > 0)
        {
            countdownDisplay.gameObject.SetActive(true);
            countdownDisplay.text = countdownNextWave.ToString();

            yield return new WaitForSeconds(1f);

            countdownNextWave--;
        }

        countdownDisplay.text = "Wave 2";

        enemyManger.GetComponent<FungiSpawner>().SpawnTwoFungi(10, 15);

        yield return new WaitForSeconds(3f);

        countdownDisplay.gameObject.SetActive(false);
    }
*/

}
