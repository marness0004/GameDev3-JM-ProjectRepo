using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TreeHP : MonoBehaviour
{
    public Text treeHP;
    public Text gameOver;

    private int counter = 3;

    public void Start()
    {
        treeHP.text = "Tree Health: " + counter.ToString();
        gameOver.gameObject.SetActive(false);
    }

    public void TreeHit()
    {
        counter--;
        treeHP.text = "Tree Health: " + counter.ToString();
    }

    public void TreeEndGame()
    {
        SceneManager.LoadScene("Scenes/MainMenu");
    }

    public void Update()
    {
        if ( 0 >= counter )
        {
            gameOver.gameObject.SetActive(true);
            Invoke("TreeEndGame", 5.0f);
        }
        //Debug.Log(counter.ToString());
    }

}
