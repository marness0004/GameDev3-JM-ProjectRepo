using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    public GameObject player;
    public WeaponManger weaponMangerScript;
    public bool pickupGun1;
    public bool pickupGun2;
    public bool pickupGun3;

    void Start()
    {
        weaponMangerScript = player.gameObject.GetComponent<WeaponManger>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && pickupGun1 == true)
        {
            weaponMangerScript.Gun1Handle();
            Destroy(this.gameObject);
        }

        if (collision.gameObject.CompareTag("Player") && pickupGun2 == true)
        {
            Destroy(this.gameObject);
        }

        if (collision.gameObject.CompareTag("Player") && pickupGun3 == true)
        {
            Destroy(this.gameObject);
        }
    }
}
