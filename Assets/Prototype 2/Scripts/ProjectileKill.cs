using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileKill : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
