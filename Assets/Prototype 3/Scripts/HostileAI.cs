using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileAI : MonoBehaviour
{

    public GameObject target;
    public GameObject player;
    public int speed;
    bool isDead = false;

    void Start()
    {
        target = GameObject.FindWithTag("Target");
        player = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        transform.RotateAround(target.transform.position, Vector3.up, speed * Time.deltaTime);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && isDead == false)
        {
            isDead = true;
            player.GetComponent<RotateAround>().EndGame();
            Destroy(this.gameObject);
        }
    }
}