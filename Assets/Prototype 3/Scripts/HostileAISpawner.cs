using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HostileAISpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject enemyAIPrefab;
    public CountdownTimer2 timer;
    public Text waveCount;

    private int countdownCounter = 0;
    private int minAmount;
    private int maxAmount;

    public Transform startTransform;
    public Transform destinationTransform;

    public void RoundStart()
    {
        countdownCounter++;
        waveCount.text = "Wave " + countdownCounter.ToString();
        WaveCheck();
    }

    public void WaveCheck()
    {
        minAmount = countdownCounter * Random.Range(5, 10) / 2;
        maxAmount = countdownCounter * Random.Range(10, 15) / 2;
        Debug.Log(minAmount);
        Debug.Log(maxAmount);
        SpawnAICal();
    }

    public void SpawnAICal()
    {
        int spawnCount = Random.Range(minAmount, maxAmount);
        for (int i = 0; i < spawnCount; i++)
        {
            Invoke("SpawnAI", Random.Range(2.0f, 5.0f));
        }
    }

    public void SpawnAI()
    {
        Vector3 dir = (destinationTransform.position - startTransform.position);
        float maxDistance = dir.magnitude;
        Vector3 SpawnPosition = dir.normalized * Random.Range(0f, maxDistance);

        //#######

        GameObject fungiInstance;
        fungiInstance = Instantiate(enemyAIPrefab, SpawnPosition, Quaternion.identity);
    }
}

