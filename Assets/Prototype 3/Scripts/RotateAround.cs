using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RotateAround : MonoBehaviour
{
    public GameObject center;
    public GameObject player;
    public GameObject target;
    public GameObject enemyManger;
    public GameObject wheels;
    public Transform wheelCenter;
    public CountdownTimer2 timer;
    public Text gameover;
    public Text gameoverCondition;
    public int speed;
    public float speedD;
    public float speedA;

    public bool isLeanLeft = false;
    public bool isLeanRight = false;
    public bool right = false;
    public bool left = false;
    public bool isHit = false;
    public bool spawnCheck = false;

    public void Start()
    {
        gameover.gameObject.SetActive(false);
        gameoverCondition.gameObject.SetActive(false);
    }


    public void EndGame()
    {
        timer.gameStillRuns = false;
        gameover.gameObject.SetActive(true);
        gameoverCondition.gameObject.SetActive(true);
        Invoke("End", 5.0f);
    }

    public void OneWaveTrigger()
    {
        enemyManger.GetComponent<HostileAISpawner>().RoundStart();
        spawnCheck = false;
    }

    public void End()
    {
        SceneManager.LoadScene("Scenes/MainMenu");
    }

    void Update()
    {
        center.transform.RotateAround(target.transform.position, Vector3.up, speed * Time.deltaTime);
       
       //wheels.transform.rotate(new Vector3(0, 0, 20 * Time.deltaTime));
       //rodangle = new Vector3(0, 0, 20);
       //wheels.transform.rotation = Quaternion.Euler(rodangle);

        if (Input.GetKey(KeyCode.D))
        {
            center.transform.Translate(Vector3.left * speedD * Time.deltaTime);
            isLeanRight = true;
            if(isLeanRight == true && right == false)
            {
                player.transform.Rotate(new Vector3(0, 20, 0));//* Time.deltaTime);
                isLeanRight = false;
                right = true;
            }
        }

        //#######

        if (Input.GetKeyUp(KeyCode.D))
        {
            player.transform.Rotate(new Vector3(0, -20, 0));//* Time.deltaTime);
            right = false;
            isLeanRight = false;
        }

        //#######
        //#######
        //#######

        if (Input.GetKey(KeyCode.A))
        {
            center.transform.Translate(Vector3.right * speedA * Time.deltaTime);
            isLeanLeft = true;
            if (isLeanLeft == true && left == false)
            {
                player.transform.Rotate(new Vector3(0, -20, 0));//* Time.deltaTime);
                isLeanLeft = false;
                left = true;
            }
        }

        //#######

        if (Input.GetKeyUp(KeyCode.A))
        {
            player.transform.Rotate(new Vector3(0, 20, 0));//* Time.deltaTime);
            left = false;
            isLeanLeft = false;
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Target") && isHit == false)
        {
            isHit = true;
            gameoverCondition.text = "You Hit The Circle Mecca";
            EndGame();
        }
        if (collision.gameObject.CompareTag("Wall") && isHit == false)
        {
            isHit = true;
            gameoverCondition.text = "You Hit The Walls Of Circle Mecca";
            EndGame();
        }
        if (collision.gameObject.CompareTag("Finish") && spawnCheck == false)
        {
            spawnCheck = true;
            Invoke("OneWaveTrigger", 1.0f);
        }
    }
}

/*
 
    if (Input.GetKeyDown(KeyCode.Q))
        {
            if (isLeanLeft == false && isLeanRight == false)
            {
                transform.Rotate(new Vector3(0, 0, -10));//* Time.deltaTime);
                isLeanLeft = true;
            }
            else if (isLeanRight == true)
            {
                transform.Rotate(new Vector3(0, 0, -10));//* Time.deltaTime);
                isLeanRight = false;
            }
        }

        //#######

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (isLeanRight == false && isLeanLeft == false)
            {
                transform.Rotate(new Vector3(0, 0, 10));// * Time.deltaTime);
                isLeanRight = true;
            }
            else if (isLeanLeft == true)
            {
                isLeanLeft = false;
                transform.Rotate(new Vector3(0, 0, 10));// * Time.deltaTime);
            }
        }

 */
