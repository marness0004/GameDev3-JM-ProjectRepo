using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer2 : MonoBehaviour
{
    
    public float scoreCounter = 0;
    public bool gameStillRuns = true;
    public Text scoreCounterText;
    public GameObject enemyManger;

    public void Update()
    {
        if (gameStillRuns == true)
        {
            scoreCounter += Time.deltaTime;
        }

        scoreCounterText.text = "Score: " + scoreCounter.ToString("N2");
    }


    /*    
        public IEnumerator CountdownToWaveTwo()
        {    
            while (countdownNextWave > 0)
            {
                countdownDisplay.gameObject.SetActive(true);
                countdownDisplay.text = countdownNextWave.ToString();

                yield return new WaitForSeconds(1f);

                countdownNextWave--;
            }

            countdownDisplay.text = "Wave 2";

            enemyManger.GetComponent<FungiSpawner>().SpawnTwoFungi(10, 15);

            yield return new WaitForSeconds(3f);

            countdownDisplay.gameObject.SetActive(false);
        }
    */

    /*

            public float countdownTime;
        public float countdownNextWave;
        public float scoreCounter = 0;
        public bool gameStillRuns = true;
        public Text countdownDisplay;
        public Text scoreCounterText;
        public GameObject enemyManger;

        public void Update()
        {
            countdownTime -= Time.deltaTime;

            if (countdownTime < 0.0f)
            {
                enemyManger.GetComponent<HostileAISpawner>().RoundStart();
                ResetTimer();
            }

            if (gameStillRuns == true)
            {
                scoreCounter += Time.deltaTime;
            }

            countdownDisplay.text = countdownTime.ToString("N2");
            scoreCounterText.text = "Score: " + scoreCounter.ToString("N2");
        }

        public void ResetTimer()
        {
            countdownTime = countdownNextWave;
        }

    */

}
