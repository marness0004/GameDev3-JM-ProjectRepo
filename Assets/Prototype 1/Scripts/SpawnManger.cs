using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManger : MonoBehaviour
{

    public GameObject obstcalePrefab;
    public GameObject wallPrefab;
    private Vector3 spawnPos = new Vector3(10, 0, 0);
    private PlayerController PlayerController;
    private float randomSelect;
    private float startDelay = 5;
    private float repeatRate = 3;
    
    void Start()
    {
        InvokeRepeating("SpawnSystem", startDelay, repeatRate);
        PlayerController = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    void SpawnSystem()
    {
        randomSelect = Random.Range(1, 6);
        
        if(randomSelect == 1)
        {
            SpawnObstcale();
        }
        if (randomSelect == 2)
        {
            SpawnObstcale();
        }
        if (randomSelect == 3)
        {
            SpawnWall();
        }
        if (randomSelect == 4)
        {
            SpawnObstcale();
        }
        if (randomSelect == 5)
        {
            SpawnObstcale();
        }
        if (randomSelect == 6)
        {
            SpawnWall();
        }
    }

    void SpawnObstcale()
    {
        if (PlayerController.gameOver == false)
        Instantiate(obstcalePrefab, spawnPos, obstcalePrefab.transform.rotation);
    }

    void SpawnWall()
    {
        if (PlayerController.gameOver == false)
        Instantiate(wallPrefab, spawnPos, obstcalePrefab.transform.rotation);
    }
}
