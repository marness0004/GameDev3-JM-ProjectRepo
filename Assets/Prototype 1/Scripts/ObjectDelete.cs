using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDelete : MonoBehaviour
{
    public float objDeleteTimer;
    private float deleteTime;


    void Update()
    {
        deleteTime -= Time.deltaTime;

        if (deleteTime < objDeleteTimer)
        {
            Destroy(gameObject);
        }
    }
}
