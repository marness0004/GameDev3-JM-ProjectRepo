using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 10f;
    public Rigidbody rigidBody;

    void Start()
    {
        rigidBody.AddForce(transform.forward * speed);
    }


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            Destroy(other.gameObject);
        }
        Destroy(gameObject);
    }
}
