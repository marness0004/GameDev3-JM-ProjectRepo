using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRB;
    private Animator playerAnim;
    private AudioSource playerAudio;
    public AudioClip jumpSound;
    public AudioClip crashSound;
    public float speed = 100;
    public float jumpForce;
    public float gravityModifier;
    public bool isOnGround = true;
    public bool gameOver = false;
    public bool dirtPartStart = true;
    public ParticleSystem jumpParticle;
    public ParticleSystem landDirt;

    public GameObject projectilePrefab;
    public float projectileSpeed = 1000;
    public Transform firingPoint;


    void Start()
    {
        Physics.gravity *= gravityModifier;
        playerRB = GetComponent<Rigidbody>();
        playerAnim = GetComponent<Animator>();        
        playerAudio = GetComponent<AudioSource>();
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround)
        {
            playerRB.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isOnGround = false;
            Invoke("MakeGroundAgain", 1.2f);
            playerAnim.SetTrigger("Jump_Trig");
            jumpParticle.Play();
            landDirt.Stop();
            landDirt.gameObject.SetActive(false);
            playerAudio.PlayOneShot(jumpSound, 1.0f);
        }
        if (Input.GetMouseButtonDown(0))
        {
            GameObject projectileIstance;
            projectileIstance = Instantiate(projectilePrefab, firingPoint.position, firingPoint.rotation);
            projectileIstance.GetComponent<Rigidbody>().AddForce(firingPoint.forward * projectileSpeed);
            Destroy(projectileIstance, 5);
        }
    }

    public void Death()
    {
        SceneManager.LoadScene("Scenes/MainMenu");
    }

    public void MakeGroundAgain()
    {
        isOnGround = true;
        jumpParticle.Stop();
        landDirt.gameObject.SetActive(true);
    }

    private void OnCollisionEnter(Collision other)
    {   
        if (other.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
            landDirt.Play();
        }
        if (other.gameObject.CompareTag("Obstacle"))
        {
            playerAudio.Stop();
            playerAudio.PlayOneShot(crashSound, 1.0f);
            gameOver = true;
            playerAnim.SetTrigger("Death");
            Debug.Log("Game Over!");
            Invoke("Death", 5.0f);

        }
        else if (other.gameObject.CompareTag("Wall"))
        {
            playerAudio.Stop();
            playerAudio.PlayOneShot(crashSound, 1.0f);
            gameOver = true;
            playerAnim.SetTrigger("Death");
            Debug.Log("Game Over!");
            Invoke("Death", 5.0f);
        }
    }
}
