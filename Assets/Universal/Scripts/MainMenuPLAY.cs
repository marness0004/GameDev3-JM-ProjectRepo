using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenuPLAY : MonoBehaviour
{
    public void Game1()
    {
        SceneManager.LoadScene("Scenes/FirstUnityGame");
    }
    public void Game2()
    {
        SceneManager.LoadScene("Scenes/Tree Game");
    }
    public void Game3()
    {
        SceneManager.LoadScene("Scenes/Game3");
    }
    public void Game4()
    {
        SceneManager.LoadScene("Scenes/Game4");
    } 
    public void QuitGame()
    {
        SceneManager.LoadScene("Quit");
    }
}