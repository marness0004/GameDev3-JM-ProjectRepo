using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolPickup : MonoBehaviour
{

    public GameObject playerScript;
    public GameObject thisObject;
    public HandleManger handleManger;
    public bool pickTool1;
    public float rotationSpeed;

    void Start()
    {
        handleManger = playerScript.gameObject.GetComponent<HandleManger>();
    }

    public void Update()
    {
        thisObject.transform.Rotate(new Vector3(0, rotationSpeed * Time.deltaTime, 0));
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && pickTool1 == true)
        {
            handleManger.PickaxeHandle1();
            Destroy(thisObject);
        }
    }
}
