using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickToolTip : MonoBehaviour
{
    public HandleManger mangerScript;
    public OreDrop oreDropScriptSelect;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Rock") && mangerScript.actionIsRunning == true)
        {
            oreDropScriptSelect = collision.gameObject.GetComponent<OreDrop>();
            oreDropScriptSelect.pickHitPoint = this.gameObject.transform;
            oreDropScriptSelect.DropOre();
            //Debug.Log("Hit with Pick");
        }

        if (collision.gameObject.CompareTag("Tree") && mangerScript.actionIsRunning == true)
        {
            oreDropScriptSelect = collision.gameObject.GetComponent<OreDrop>();
            oreDropScriptSelect.pickHitPoint = this.gameObject.transform;
            oreDropScriptSelect.DropOre();
            //Debug.Log("Hit with Pick");
        }
    }

}
