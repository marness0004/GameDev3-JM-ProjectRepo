using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragItem : MonoBehaviour, IDropHandler
{

    public Canvas canvas;
    public CanvasGroup canvasGroup;

    public void DragPickup(BaseEventData data)
    {
        canvasGroup.blocksRaycasts = false;
        Debug.Log("Pickup");
    }

    public void DragHandler(BaseEventData data)
    {
        PointerEventData pointerData = (PointerEventData)data;

        Vector2 position;
        RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)canvas.transform, pointerData.position, canvas.worldCamera, out position);
        transform.position = canvas.transform.TransformPoint(position);
    }

    public void DragDrop(BaseEventData data)
    {
        canvasGroup.blocksRaycasts = true;
        Debug.Log("drop");
    }

    public void OnDrop(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

}
