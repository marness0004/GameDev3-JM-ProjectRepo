using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour, IDropHandler
{
    public HandleManger handleManger;
    public GameObject UiItemSelected;
    public bool slotThree;
    public bool slotFour;
    public bool slotFive;

   public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("AttemptedDrop");
        if (eventData.pointerDrag != null)
        {
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            UiItemSelected = eventData.pointerDrag.gameObject;
            TellHandleWhatSelected();
        }
        if (eventData.pointerDrag = null)
        {
            Debug.Log("Null");
            FixSelected();
        }
    }

    public void TellHandleWhatSelected()
    {
        if (slotThree == true)
        {
            handleManger.itemInSlot3 = true;
        }

        if (slotFour == true)
        {
            handleManger.itemInSlot4 = true;
        }

        if (slotFive == true)
        {
            handleManger.itemInSlot5 = true;
        }
    }
    public void FixSelected()
    {
        if (slotThree == true)
        {
            handleManger.itemInSlot3 = false;
        }

        if (slotFour == true)
        {
            handleManger.itemInSlot4 = false;
        }

        if (slotFive == true)
        {
            handleManger.itemInSlot5 = false;
        }
    }
}
