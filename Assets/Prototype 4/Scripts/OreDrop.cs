using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreDrop : MonoBehaviour
{
    public GameObject mainOreDrop;
    public GameObject ironOreDrop;
    public GameObject woodOreDrop;
    public bool oreHasBeenSelected;
    public bool oreTypeIron;
    public bool oreTypeTreeWood;
    public float oreUpSpeed;

    public Transform pickHitPoint;


    public void Start()
    {
        if (oreTypeIron == true && oreHasBeenSelected == false)
        {
            oreHasBeenSelected = true;
            mainOreDrop = ironOreDrop;
        }

        if (oreTypeTreeWood == true && oreHasBeenSelected == false)
        {
            oreHasBeenSelected = true;
            mainOreDrop = woodOreDrop;
        }
    }

    public void DropOre()
    {
        GameObject oreDropInstance;
        Transform oreDropPoint;
        oreDropInstance = Instantiate(mainOreDrop, pickHitPoint.position, pickHitPoint.rotation);
        oreDropPoint = mainOreDrop.transform;
        oreDropInstance.GetComponent<Rigidbody>().AddForce(oreDropPoint.up * oreUpSpeed);
    }

}

//new Vector3(transform.position.x, -22, transform.position.z), transform.rotation
