using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Crafting : MonoBehaviour
{
    public HandleManger handleManger;
    public Text gameover;

    public void Start()
    {
        gameover.gameObject.SetActive(false);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Obstacle")
        {
            gameover.gameObject.SetActive(true);
            Invoke("End", 5.0f);
        }
    }

    public void End()
    {
        SceneManager.LoadScene("Scenes/MainMenu");
    }

    //#######################
    //### Access Crafting ###
    //#######################

    void OnTriggerEnter(Collider collision)
    {
        //Debug.Log("Hit");
        if (collision.gameObject.tag == "Target")
        {
            handleManger.canCraft = true;
            handleManger.pressToCraftUI.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        //Debug.Log("Exit");
        if (collision.gameObject.tag == "Target")
        {
            handleManger.canCraft = false;
            handleManger.pressToCraftUI.gameObject.SetActive(false);
        }
    }
}
