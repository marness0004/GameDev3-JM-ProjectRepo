using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrePickup : MonoBehaviour
{
    public HandleManger handleManger;
    public bool collectDelay = false;
    public int stoneRandomCount;
    public int woodRandomCount;

    public void OnCollisionEnter(Collision playerCollision)
    {
        if (playerCollision.gameObject.CompareTag("IronOre") && collectDelay == false)
        {
            collectDelay = true;
            Destroy(playerCollision.gameObject);
            handleManger.inventoryIronOreCounter++;
            stoneRandomCount = Random.Range(2, 4);
            handleManger.inventoryStoneCounter = handleManger.inventoryStoneCounter + stoneRandomCount;
            Invoke("FixCounter", 0.5f);
        }

        if (playerCollision.gameObject.CompareTag("WoodOre") && collectDelay == false)
        {
            collectDelay = true;
            Destroy(playerCollision.gameObject);
            woodRandomCount = Random.Range(1, 3);
            handleManger.inventoryWoodCounter = handleManger.inventoryWoodCounter + woodRandomCount;
            Invoke("FixCounter", 0.5f);
        }
    }

    public void FixCounter()
    {
        collectDelay = false;
    }

}
